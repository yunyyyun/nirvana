//
//  AppDelegate.h
//  Nirvana
//
//  Created by DFG on 2019/2/20.
//  Copyright © 2019 mengyun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

