//
//  MYColorConfig.m
//  Nirvana
//
//  Created by DFG on 2019/2/20.
//  Copyright © 2019 mengyun. All rights reserved.
//

#import "MYColorConfig.h"

@implementation MYColorConfig

+ (instancetype)sharedObjcet
{
    static MYColorConfig *config = nil;
    @synchronized(self){
    }
    
    return config;
}

- (UIColor *)customDarkGray{
    return [UIColor colorWithRed:12/255.0 green:22/155.0 blue:12/255.0 alpha:1.0];
}

@end
