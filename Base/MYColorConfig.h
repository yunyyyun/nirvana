//
//  MYColorConfig.h
//  Nirvana
//
//  Created by DFG on 2019/2/20.
//  Copyright © 2019 mengyun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MYColorConfig : NSObject

@property (nonatomic, strong, readonly) UIColor *customDarkGray;

+ (instancetype)sharedObjcet;

@end

NS_ASSUME_NONNULL_END
