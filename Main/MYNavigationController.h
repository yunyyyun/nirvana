//
//  MYNavigationController.h
//  Nirvana
//
//  Created by DFG on 2019/2/20.
//  Copyright © 2019 mengyun. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RootNavigationController : UINavigationController

@end

@interface MYNavigationController : RootNavigationController

@end

NS_ASSUME_NONNULL_END
